class Chatwork {
  constructor() {
    this.room = window.wrappedJSObject.RM;
    this.account = window.wrappedJSObject.AC;
    this.roomList = window.wrappedJSObject.RL;
  }

  wrapJSObject() {
    XPCNativeWrapper(this.room);
    XPCNativeWrapper(this.account);
    XPCNativeWrapper(this.roomList);
  }

  getRoomMembers() {
    let roomMemberIds = this.room.getSortedMemberList();

    let roomMembers = [];
    for (const memberId of roomMemberIds) {
      roomMembers.push({
        'id': memberId,
        'name': this.account.getDefaultNickName(memberId)
      });
    }

    this.wrapJSObject();
    return roomMembers;
  }

  shouldChangeRoom(newRoomId) {
    return this.room.id !== newRoomId;
  }
}

let chatwork = new Chatwork();
module.exports = chatwork;