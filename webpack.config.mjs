import WebExtPlugin from 'web-ext-plugin';

export default {
  entry: './src/index.js',
  output: {
    filename: 'content.js'
  },
  plugins: [
    new WebExtPlugin({})
  ],
};